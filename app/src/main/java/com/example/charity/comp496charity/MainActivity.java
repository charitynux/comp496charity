package com.example.charity.comp496charity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button thirdButton,secondButton,firstButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        thirdButton=(Button)findViewById(R.id.thirdButton);
        secondButton=(Button)findViewById(R.id.secondButton);
        firstButton=(Button)findViewById(R.id.firstButton);

        //Event to listen when the Third Button is pressed
        thirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thirdActivity();
            }
        });

        //Event to listen when the Second Button is pressed
        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                secondActivity();
            }
        });

        //Event to listen when the first button is pressed
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstActivity();
            }
        });
    }

    public void thirdActivity(){

        Intent i= new Intent(this,third.class);
        startActivity(i);

    }

    public void secondActivity(){
        Intent intent=new Intent(this,second.class);
        startActivity(intent);
    }

    public void firstActivity(){
    Intent v= new Intent(this,first.class);
        startActivity(v);
    }
}
